const txtFirstName =  document.querySelector('#txt-first-name')
const spanFullName = document.querySelector('#span-full-name')

const txtLastName = document.querySelector('#txt-last-name')
//const spanLastName = document.querySelector('#span-last-name')


/*txtFirstName.addEventListener('keyup',event=>{
	spanFullName.innerHTML = txtFirstName.value
})

txtLastName.addEventListener('keyup',event=>{
	spanLastName.innerHTML = txtLastName.value
})


txtFirstName.addEventListener('keyup',event=>{
	console.log(event.target)
	console.log(event.target.value)
})

txtLastName.addEventListener('keyup',event=>{
	console.log(event.target)
	console.log(event.target.value)
})

*/
// Sr. Alvin's Solution
const updateFullName = () => {
    let firstName = txtFirstName.value;
    let lastName = txtLastName.value;

    spanFullName.innerHTML = `${firstName} ${lastName}`;
}

txtLastName.addEventListener('keyup', updateFullName);
txtFirstName.addEventListener('keyup', updateFullName);
